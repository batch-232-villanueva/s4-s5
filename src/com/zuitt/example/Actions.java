package com.zuitt.example;

public interface Actions {
    // interface - blueprints for your class that implements our interface MUST have the methods in the interface
    public void sleep();
    public void run();
}
