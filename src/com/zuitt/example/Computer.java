package com.zuitt.example;
// implements Actions.java in Computer and instantiate in Main.java, run the methods
public class Computer implements Actions {
    public void sleep(){
        System.out.println("Computer goes to sleep...");
    }
    public void run(){
        System.out.println("Computer is running!");
    }
}
